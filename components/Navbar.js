import React, {useState} from 'react';
import Link from "next/link";
import Image from "next/image";

const Navbar = () => {
    const [open, setOpen] = useState(false);
    return (

        <nav className="flex filter drop-shadow-md bg-black text-white px-4 py-4 h-20 justify-around items-center">
            {/*<MobileNav open={open} setOpen={setOpen}/>*/}
            <Link href="/">
                <div id="logo" className="flex items-center hover:cursor-pointer">
                    <Image width="50px" height="50px" src="/logo.png" alt="restaurant logo"/>
                    <p className="text-[20px] text-white mx-3">My Restaurant</p>
                </div>
            </Link>
            <div className="w-9/12 flex justify-end items-center">

                <div className="z-50 flex  relative w-8 h-8 flex-col justify-between items-center md:hidden"
                     onClick={() => {
                         setOpen(!open)
                     }}>
                    {/* hamburger button */}
                    <span
                        className={`h-1 w-full bg-white rounded-lg transform transition duration-300 ease-in-out ${open ? "rotate-45 translate-y-3.5" : ""}`}/>
                    <span
                        className={`h-1 w-full bg-white rounded-lg transition-all duration-300 ease-in-out ${open ? "w-0" : "w-full"}`}/>
                    <span
                        className={`h-1 w-full bg-white rounded-lg transform transition duration-300 ease-in-out ${open ? "-rotate-45 -translate-y-3.5" : ""}`}/>
                </div>

                <div className="hidden md:flex items-center">
                    <div id="menu" className="w-96 flex justify-around">
                        <Link href="#menu">Menu</Link>
                        <Link href="/order">My orders</Link>
                        <Link href="/about">About</Link>
                        <Link href="/contact">Contact</Link>
                    </div>
                </div>
                <div id="search-bar">
                    <label className="relative block">
                        <span className="sr-only">Search</span>
                        <span className="absolute inset-y-0 left-0 flex items-center pl-2">
                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="25px"
                         height="25px"><path
                        d="M 21 3 C 11.621094 3 4 10.621094 4 20 C 4 29.378906 11.621094 37 21 37 C 24.710938 37 28.140625 35.804688 30.9375 33.78125 L 44.09375 46.90625 L 46.90625 44.09375 L 33.90625 31.0625 C 36.460938 28.085938 38 24.222656 38 20 C 38 10.621094 30.378906 3 21 3 Z M 21 5 C 29.296875 5 36 11.703125 36 20 C 36 28.296875 29.296875 35 21 35 C 12.703125 35 6 28.296875 6 20 C 6 11.703125 12.703125 5 21 5 Z"/></svg>
                    </span>
                        <input
                            className="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm text-black"
                            placeholder="Search..." type="text" name="search"/>
                    </label>
                </div>
                <div className="flex items-center justify-between w-48">

                    <div id="header-icons" className="w-24 flex justify-around">
                        <i className="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>
                        <i className="fa fa-bell fa-lg" aria-hidden="true"></i>
                    </div>
                    <div id="profile-image" className="w-14 h-14 border rounded-full overflow-hidden">
                        <img className="object-cover object-center" src="/morgan.png" alt="profile-image"/>
                    </div>
                </div>

            </div>


        </nav>

    )
        ;
};

const MobileNav = ({open, setOpen}) => {
    return (
        <div
            className={`absolute top-0 left-0 h-screen w-screen bg-black transform ${open ? "-translate-x-0" : "-translate-x-full"} transition-transform duration-300 ease-in-out filter drop-shadow-md `}>
            <div className="flex items-center justify-center filter drop-shadow-md  h-20"> {/*logo container*/}
                <Link className="text-xl font-semibold" href="/">LOGO</Link>
            </div>
            <div className="flex flex-col ml-4">
                <a className="text-xl font-medium my-4" href="/about" onClick={() => setTimeout(() => {
                    setOpen(!open)
                }, 100)}>
                    About
                </a>
                <Link className="text-xl font-normal my-4" href="/contact" onClick={() => setTimeout(() => {
                    setOpen(!open)
                }, 100)}>
                    Contact
                </Link>
            </div>
        </div>
    )
}

export default Navbar;