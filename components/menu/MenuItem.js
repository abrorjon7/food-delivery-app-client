import React from 'react';

const MenuItem = (props) => {

    function handleAddToCartBtn() {
        alert("Clicked!!!")
    }

    const {id, name, price, imgUrl} = props;
    return (
        <div id="menu-item"
             className="h-48 md:h-96 w-32 md:w-72 shadow-md border mt-14"
        >
            <img className="mx-auto" src={imgUrl} alt=""/>
            <div className="text-center">
                <h1>{name}</h1>
                <h3>{price}</h3>
            </div>

            <div className="text-center mt-4">
                <button onClick={() => (handleAddToCartBtn())}
                        className="px-3 md:px-14 p-1 md:p-2 rounded-full bg-blue-400 text-white">Add to cart
                </button>
            </div>

        </div>
    );
};

export default MenuItem;