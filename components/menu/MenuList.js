import React, {useState} from 'react';
import MenuItem from "./MenuItem";

const MenuList = () => {

    const [foodList, setFoodList] = useState([
        {
            'id': 1,
            'name': 'Sandwiches',
            'foods': [
                {
                    'id': 1,
                    'name': 'Burger',
                    'price': 22000,
                    'imgUrl': '/burger.png',
                },
                {
                    'id': 2,
                    'name': 'Burger 2',
                    'price': 22000,
                    'imgUrl': '/burger.png',
                },
                {
                    'id': 3,
                    'name': 'Burger 3',
                    'price': 22000,
                    'imgUrl': '/burger.png',
                },
                {
                    'id': 4,
                    'name': 'Burger 4',
                    'price': 22000,
                    'imgUrl': '/burger.png',
                },
                {
                    'id': 5,
                    'name': 'Burger 5',
                    'price': 22000,
                    'imgUrl': '/burger.png',
                }
            ]

        },
    ]);


    return (
        <div id="menu-list">
            {foodList.map((cat, i) => (
                <div key={i}>
                    <h1 id="category" className="text-4xl font-bold">{cat.name}</h1>
                    <div className="flex flex-wrap -mx-2 overflow-hidden sm:-mx-3 md:-mx-3 lg:-mx-4 xl:-mx-5">
                        {cat.foods.map((food, j) => (
                            <div key={j}
                                 className="my-2 px-2 w-1/2 overflow-hidden sm:my-3 sm:px-3 sm:w-1/3 md:my-3
                                 md:px-3 md:w-1/3 lg:my-4 lg:px-4 lg:w-1/4 xl:my-5 xl:px-5 xl:w-1/4">
                                <MenuItem id={food.id}
                                          name={food.name}
                                          imgUrl={food.imgUrl}
                                          price={food.price}
                                />
                            </div>
                        ))}
                    </div>
                </div>

            ))

            }

        </div>
    );
};

export default MenuList;