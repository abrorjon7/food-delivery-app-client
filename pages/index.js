import Head from 'next/head'
import Navbar from "../components/Navbar";
import MenuList from "../components/menu/MenuList";


export default function Home() {
    return (
        <div>
            <Head>
                <title>Home page</title>
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
                <link rel="icon" href="/fav-icon.png"/>
            </Head>

            <Navbar/>
            <main className="container mx-auto my-14">
                <MenuList/>
            </main>


        </div>
    )
}
